package experiments;

public class SimpleTestExperiment {
	public double num1 = 10.5;
	  private double num2 = 20.7;
	  
	  public String fName = "Sachini";
	  private String lName = "Peiris";
	  

	  public SimpleTestExperiment() {
	  }

	  public SimpleTestExperiment(double x, double y) {
	    this.num1 = x;
	    this.num2 = y;
	  }

	  public void squareNum1() {
	    this.num1 *= this.num1;
	  }

	  private void squareNum2() {
	    this.num2 *= this.num2;
	  }

	  public double getX() {
	    return num1;
	  }

	  private void setX(double x) {
	    this.num1 = x;
	  }

	  public double getY() {
	    return num2;
	  }

	  public void setY(double y) {
	    this.num2 = y;
	  }

	  public String toString() {
	    return String.format("(c:%f, d:%f)", num1, num2);
	  }
	  
	  public SimpleTestExperiment(String word1, String word2) {
		    this.fName = word1;
		    this.lName = word2;
	  }
	  
	  public void stringFname() {
		   this.fName += this.fName;
	  }
	  
	  public String getFname() {
		   return fName;
	  }
	  
	  public void stringLname() {
		  this.lName += this.lName;
	  }
	  
	  public String getLname() {
		   return lName;
	  }
	  
	  public double getC() {
		  return num1;
	  }
	  private void setC(double c) {
		  this.num1 = c;
	  }
	  public double getD() {
		  return num2;
	  }
	  public void setD(double d) {
		  this.num2 = d;
	  }
}
