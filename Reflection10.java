package experiments;

import java.lang.reflect.*;

/******************************************************************************
 * The illusive private method setA is called in this example. This demonstrates
 * that all the rules of encapsulation that you learned in your introduction to
 * object oriented programming can be broken. Why would we want to do this?
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2017
 ******************************************************************************/

public class Reflection10 {
  public static void main(String[] args) throws Exception {
	SimpleTestExperiment value = new SimpleTestExperiment();
    Method m = value.getClass().getDeclaredMethod("setX", double.class);
    m.setAccessible(true);
    m.invoke(value, 80.5);
    System.out.println(value);
  }
}
