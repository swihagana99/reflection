package kunitDemo1;

import static kunit1.KUnit.*;

import experiments.*;

/******************************************************************************
 * This code demonstrates the use of the KUnit testing tool. It produces a
 * report that contains messages generated from the check methods.
 * 
 * author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimple {

  void checkConstructorAndAccess(){
    SimpleTestExperiment s = new SimpleTestExperiment(10.5, 20.7);
    checkEquals(s.getX(), 11.2);
    checkEquals(s.getY(), 11.2);
    checkNotEquals(s.getX(), 11.2);    
    checkNotEquals(s.getY(), 12.5);    
  }

  void checksquareNum1(){
    SimpleTestExperiment s = new SimpleTestExperiment(3.6, 4.7);
    s.squareNum1();
    checkEquals(s.getX(), 9.5);
  }

  public static void main(String[] args) {
    TestSimple ts = new TestSimple();
    ts.checkConstructorAndAccess();
    ts.checksquareNum1();
    report();
  }
}
