package TestExperi;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import experiments.*;

public class SimpleTestExperimentTest {

    @Test
    public void testSquareNum1() {
        SimpleTestExperiment testExperiment = new SimpleTestExperiment();
        testExperiment.squareNum1();
        double expected = 110.25;
        double actual = testExperiment.getX();
        Assert.assertEquals(expected, actual, 0.001);
    }

    @Test
    public void testSetY() {
        SimpleTestExperiment testExperiment = new SimpleTestExperiment();
        testExperiment.setY(15.3);
        double expected = 15.3;
        double actual = testExperiment.getY();
        Assert.assertEquals(expected, actual, 0.001);
    }

}
