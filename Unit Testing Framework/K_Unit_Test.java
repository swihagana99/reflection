package TestExperi;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class K_Unit_Test {
  private static List<String> checks;
  private static int checksMade = 0;
  private static int passedChecks = 0;
  private static int failedChecks = 0;

  private static void addToReport(String txt) {
    if (checks == null) {
      checks = new LinkedList<String>();
    }
    checks.add(String.format("%04d: %s", checksMade++, txt));
  }

  public static void checkEquals(double value1, double value2) {
	// Simple assertion
    if (value1 == value2) {
      addToReport(String.format("  %f == %f", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %f == %f", value1, value2));
      failedChecks++;
    }
  }

  public static void checkNotEquals(double value1, double value2) {
	// Simple assertion
    if (value1 != value2) {
      addToReport(String.format("  %f != %f", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %f != %f", value1, value2));
      failedChecks++;
    }
  
  }
  // Reporting and Logging comment
  public static void report() {
    Logger log = LogManager.getLogger(K_Unit_Test.class);
    log.debug(passedChecks + " checks passed");
    log.info(failedChecks + " checks failed");

    System.out.printf("%d checks passed\n", passedChecks);
    System.out.printf("%d checks failed\n", failedChecks);
    System.out.println();

    for (String check : checks) {
      System.out.println(check);
    }
  }
}
