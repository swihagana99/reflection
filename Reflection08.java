package experiments;

import java.lang.reflect.*;

/******************************************************************************
 * Not only can the value of a private field be accessed, but this experiment
 * shows that it can be altered!!
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2017
 ******************************************************************************/

public class Reflection08 {
  public static void main(String[] args) throws Exception {
    SimpleTestExperiment value = new SimpleTestExperiment();
    Field[] fields = value.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", fields.length);
    for (Field f : fields) {
      f.setAccessible(true);
      double x = f.getDouble(value);
      x++;
      f.setDouble(value, x);
      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
          f.getType(), f.getDouble(value));
    }
  }
}
