package TestExperi;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;

import experiments.SimpleTestExperiment;

public class experimentMainTest {
	public static void main(String args[]) throws IOException, SQLException {
		// Launcher: Main method that serves as the entry point of the program
        	K_Unit_Test.checkEquals(3.5, 3.5);
	        K_Unit_Test.checkEquals(3.5, 3.5);// Simple assertion
	        K_Unit_Test.checkNotEquals(2.5, 2.5);// Simple assertion
	        K_Unit_Test.checkEquals(2.5, 2.5);// Simple assertion

	        SimpleTestExperiment s = new SimpleTestExperiment(3.5, 2.5);

	        Field field = null;
	        try {
	            field = s.getClass().getDeclaredField("num1");
	        } catch (NoSuchFieldException | SecurityException e) {
	            e.printStackTrace();// Resilience to exceptions: Prints the stack trace if an exception occurs while getting the field
	        }
	        field.setAccessible(true);
	        try {
	            K_Unit_Test.checkEquals(field.getDouble(s), 2.225);// Complex assertion
	        } catch (IllegalArgumentException | IllegalAccessException e) {
	            e.printStackTrace();// Resilience to exceptions: Prints the stack trace if an exception occurs while accessing the field
	        }

	        K_Unit_Test.report();// Reporting and Logging comment
	    }
}
